package com.das.common;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

@MappedSuperclass
public abstract class BaseModel{	
	
	@Column(name="createdBy")
	private long createdBy;
        
    @Column(name="updatedBy")
	private long updatedBy;
        
    @Column(name="createdDate")
	private long createdDate;
        
    @Column(name="updateDate")
	private long updateDate;
    
    @Column(name = "isDeleted")
    private boolean isDeleted;

    @Column(name = "isActive")
    private boolean isActive;
	
	public long getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(long createdBy) {
		this.createdBy = createdBy;
	}
	public long getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(long updatedBy) {
		this.updatedBy = updatedBy;
	}
	public long getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(long createdDate) {
		this.createdDate = createdDate;
	}
	public long getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(long updateDate) {
		this.updateDate = updateDate;
	}	

	public boolean isDeleted() {
		return isDeleted;
	}
	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	public boolean isActive() {
		return isActive;
	}
	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}
	
	@PrePersist
    void onCreate() {
        this.setCreatedDate((new Date()).getTime());
    }

    @PreUpdate
    void onPersist() {
        this.setUpdateDate((new Date()).getTime());
    }
}