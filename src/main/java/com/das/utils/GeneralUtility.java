package com.das.utils;

import com.das.common.WsResponse;

public class GeneralUtility {

	public static WsResponse getData(Object data, String msg, int status,
			Object errors, Object config) {

		WsResponse wsResponse = new WsResponse();
		wsResponse.setData(data);
		wsResponse.setMessage(msg);
		wsResponse.setStatus(status);
		wsResponse.setErrors(errors);
		wsResponse.setConfig(config);

		return wsResponse;
	}
}