package com.das.service;

import java.util.List;

import com.das.model.Patient;
import com.das.model.User;

public interface PatientService {
	

	Patient addPatient(Patient patient) throws Exception;

	Patient editPatientById(Patient patient) throws Exception;

	void deletePatientById(Long id) throws Exception;

	Patient getPatientById(Long id) throws Exception;

	List<Patient> getAllPatients() throws Exception;

}