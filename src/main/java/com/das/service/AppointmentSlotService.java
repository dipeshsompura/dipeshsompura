package com.das.service;

import java.util.List;

import com.das.model.AppointmentSlot;
import com.das.model.User;

public interface AppointmentSlotService {
	

	AppointmentSlot addAppointmentSlot(AppointmentSlot userRequest) throws Exception;

	AppointmentSlot editAppointmentSlotById(AppointmentSlot editUserRequest) throws Exception;

	void deleteAppointmentSlotById(Long id) throws Exception;

	AppointmentSlot getAppointmentSlotById(Long id) throws Exception;

	List<AppointmentSlot> getAllAppointmentSlots() throws Exception;

}