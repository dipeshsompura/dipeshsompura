package com.das.service;

import java.util.List;

import com.das.model.User;

public interface UserService {
	
	User login(User userRequest) throws Exception;

	User addUser(User userRequest) throws Exception;

	User editUserById(User editUserRequest) throws Exception;

	void deleteUserById(Long id) throws Exception;

	User getUserById(Long id) throws Exception;

	boolean isUserExist(String userName) throws Exception;
	
	List<User> getAllUsers() throws Exception;

}