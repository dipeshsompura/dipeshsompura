package com.das.service;

import java.util.List;

import com.das.model.Doctor;

public interface DoctorService {
	
	Doctor addDoctor(Doctor doctor) throws Exception;

	Doctor editDoctorById(Doctor doctor) throws Exception;

	void deleteDoctorById(Long id) throws Exception;

	Doctor getDoctorById(Long id) throws Exception;

	List<Doctor> getAllDoctors() throws Exception;

}