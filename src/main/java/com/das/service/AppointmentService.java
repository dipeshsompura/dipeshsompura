package com.das.service;

import java.util.List;

import com.das.model.Appointment;
import com.das.model.User;

public interface AppointmentService {
	

	Appointment addAppointment(Appointment appointment) throws Exception;

	Appointment editAppointmentById(Appointment appointment) throws Exception;

	void deleteAppointmentById(Long id) throws Exception;

	Appointment getAppointmentById(Long id) throws Exception;

	List<Appointment> getAllAppointments() throws Exception;

}