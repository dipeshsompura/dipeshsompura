package com.das.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.das.model.Appointment;
import com.das.model.AppointmentSlot;
import com.das.model.User;

@Repository
public interface AppointmentRepository extends JpaRepository<Appointment, Long> {

	Appointment findById(long id);
}
