package com.das.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.das.model.AppointmentSlot;
import com.das.model.Doctor;
import com.das.model.User;

@Repository
public interface AppointmentSlotRepository extends JpaRepository<AppointmentSlot, Long> {
	AppointmentSlot findById(long id);
}
