package com.das.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.das.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

	User findByEmail(String useranme);
	User findById(long id);
	User findByEmailAndPassword(String email, String password);
}
