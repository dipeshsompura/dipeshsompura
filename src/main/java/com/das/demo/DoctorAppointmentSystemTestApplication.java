package com.das.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DoctorAppointmentSystemTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(DoctorAppointmentSystemTestApplication.class, args);
	}

}
