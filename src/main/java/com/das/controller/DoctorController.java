package com.das.controller;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.das.common.WsResponse;
import com.das.model.Doctor;
import com.das.service.DoctorService;
import com.das.utils.GeneralUtility;

@RestController
public class DoctorController {
	
	private static final Logger logger = LoggerFactory.getLogger(DoctorController.class);

	@Autowired
	private DoctorService doctorService;
	
	@PostMapping("/doctor")
	public WsResponse addDoctor(@RequestBody Doctor doctor, HttpServletRequest req) throws Exception {
		return GeneralUtility.getData(doctorService.addDoctor(doctor), "", 200, new ArrayList<>(), new ArrayList<>());
	}

	@PutMapping("/doctor")
	public WsResponse editDoctor(@RequestBody Doctor doctor, HttpServletRequest req) throws Exception {
		return GeneralUtility.getData(doctorService.editDoctorById(doctor), "", 200, new ArrayList<>(),
				new ArrayList<>());
	}

	@GetMapping("/doctor/{id}")
	public WsResponse getDoctorById(@PathVariable Long id, HttpServletRequest req) throws Exception {
		return GeneralUtility.getData(doctorService.getDoctorById(id), "", 200, new ArrayList<>(), new ArrayList<>());
	}

	@DeleteMapping("/doctor/{id}")
	public WsResponse deleteDoctor(@PathVariable Long id, HttpServletRequest req) throws Exception {
		doctorService.deleteDoctorById(id);
		return GeneralUtility.getData("", "", 200, new ArrayList<>(), new ArrayList<>());
	}
	
	@GetMapping("/doctor")
	public WsResponse getAllDoctors(HttpServletRequest req) throws Exception {
		return GeneralUtility.getData(doctorService.getAllDoctors(), "", 200, new ArrayList<>(), new ArrayList<>());
	}

}
