package com.das.controller;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.das.common.WsResponse;
import com.das.model.User;
import com.das.service.UserService;
import com.das.utils.GeneralUtility;

@RestController
public class UserController {
	
	private static final Logger logger = LoggerFactory.getLogger(UserController.class);

	@Autowired
	private UserService userService;
	
	@PostMapping("/login")
	public WsResponse loginUser(@RequestBody User userRequest, HttpServletRequest req) throws Exception {
		return GeneralUtility.getData(userService.login(userRequest), "", 200, new ArrayList<>(), new ArrayList<>());
	}

	@PostMapping("/user")
	public WsResponse addUser(@RequestBody User userRequest, HttpServletRequest req) throws Exception {
		return GeneralUtility.getData(userService.addUser(userRequest), "", 200, new ArrayList<>(), new ArrayList<>());
	}

	@PutMapping("/user")
	public WsResponse editUser(@RequestBody User userRequest, HttpServletRequest req) throws Exception {
		return GeneralUtility.getData(userService.editUserById(userRequest), "", 200, new ArrayList<>(),
				new ArrayList<>());
	}

	@GetMapping("/user/{id}")
	public WsResponse getUserById(@PathVariable Long id, HttpServletRequest req) throws Exception {
		return GeneralUtility.getData(userService.getUserById(id), "", 200, new ArrayList<>(), new ArrayList<>());
	}

	@DeleteMapping("/user/{id}")
	public WsResponse deleteUsers(@PathVariable Long id, HttpServletRequest req) throws Exception {
		userService.deleteUserById(id);
		return GeneralUtility.getData("", "", 200, new ArrayList<>(), new ArrayList<>());
	}
	
	@GetMapping("/users")
	public WsResponse getAllUsers(HttpServletRequest req) throws Exception {
		return GeneralUtility.getData(userService.getAllUsers(), "", 200, new ArrayList<>(), new ArrayList<>());
	}

}
