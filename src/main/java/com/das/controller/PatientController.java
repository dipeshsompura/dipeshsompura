package com.das.controller;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.das.common.WsResponse;
import com.das.model.Patient;
import com.das.model.User;
import com.das.service.PatientService;
import com.das.utils.GeneralUtility;

@RestController
public class PatientController {
	
	private static final Logger logger = LoggerFactory.getLogger(PatientController.class);

	@Autowired
	private PatientService patientService;
	
	@PostMapping("/patient")
	public WsResponse addPatient(@RequestBody Patient patient, HttpServletRequest req) throws Exception {
		return GeneralUtility.getData(patientService.addPatient(patient), "", 200, new ArrayList<>(), new ArrayList<>());
	}

	@PutMapping("/patient")
	public WsResponse editPatient(@RequestBody Patient patient, HttpServletRequest req) throws Exception {
		return GeneralUtility.getData(patientService.editPatientById(patient), "", 200, new ArrayList<>(),
				new ArrayList<>());
	}

	@GetMapping("/patient/{id}")
	public WsResponse getPatientById(@PathVariable Long id, HttpServletRequest req) throws Exception {
		return GeneralUtility.getData(patientService.getPatientById(id), "", 200, new ArrayList<>(), new ArrayList<>());
	}

	@DeleteMapping("/patient/{id}")
	public WsResponse deletePatient(@PathVariable Long id, HttpServletRequest req) throws Exception {
		patientService.deletePatientById(id);
		return GeneralUtility.getData("", "", 200, new ArrayList<>(), new ArrayList<>());
	}
	
	@GetMapping("/patients")
	public WsResponse getAllPatients(HttpServletRequest req) throws Exception {
		return GeneralUtility.getData(patientService.getAllPatients(), "", 200, new ArrayList<>(), new ArrayList<>());
	}

}
