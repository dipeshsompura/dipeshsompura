package com.das.controller;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.das.common.WsResponse;
import com.das.model.AppointmentSlot;
import com.das.service.AppointmentSlotService;
import com.das.utils.GeneralUtility;

@RestController
public class AppointmentSlotController {
	
	private static final Logger logger = LoggerFactory.getLogger(AppointmentSlotController.class);

	@Autowired
	private AppointmentSlotService appointmentSlotService;
	
	@PostMapping("/appointmentslot")
	public WsResponse addAppointmentSlot(@RequestBody AppointmentSlot appointment, HttpServletRequest req) throws Exception {
		return GeneralUtility.getData(appointmentSlotService.addAppointmentSlot(appointment), "", 200, new ArrayList<>(), new ArrayList<>());
	}

	@PutMapping("/appointmentslot")
	public WsResponse editAppointmentSlot(@RequestBody AppointmentSlot appointment, HttpServletRequest req) throws Exception {
		return GeneralUtility.getData(appointmentSlotService.editAppointmentSlotById(appointment), "", 200, new ArrayList<>(),
				new ArrayList<>());
	}

	@GetMapping("/appointmentslot/{id}")
	public WsResponse getAppointmentSlotById(@PathVariable Long id, HttpServletRequest req) throws Exception {
		return GeneralUtility.getData(appointmentSlotService.getAppointmentSlotById(id), "", 200, new ArrayList<>(), new ArrayList<>());
	}

	@DeleteMapping("/appointmentslot/{id}")
	public WsResponse deleteAppointmentSlot(@PathVariable Long id, HttpServletRequest req) throws Exception {
		appointmentSlotService.deleteAppointmentSlotById(id);
		return GeneralUtility.getData("", "", 200, new ArrayList<>(), new ArrayList<>());
	}
	
	@GetMapping("/appointmentslot")
	public WsResponse getAllAppointmentSlots(HttpServletRequest req) throws Exception {
		return GeneralUtility.getData(appointmentSlotService.getAllAppointmentSlots(), "", 200, new ArrayList<>(), new ArrayList<>());
	}
}
