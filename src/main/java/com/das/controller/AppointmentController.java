package com.das.controller;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.das.common.WsResponse;
import com.das.model.Appointment;
import com.das.service.AppointmentService;
import com.das.utils.GeneralUtility;

@RestController
public class AppointmentController {
	
	private static final Logger logger = LoggerFactory.getLogger(AppointmentController.class);

	@Autowired
	private AppointmentService appointmentService;
	
	@PostMapping("/appointment")
	public WsResponse addAppointment(@RequestBody Appointment appointment, HttpServletRequest req) throws Exception {
		return GeneralUtility.getData(appointmentService.addAppointment(appointment), "", 200, new ArrayList<>(), new ArrayList<>());
	}

	@PutMapping("/appointment")
	public WsResponse editAppointment(@RequestBody Appointment appointment, HttpServletRequest req) throws Exception {
		return GeneralUtility.getData(appointmentService.editAppointmentById(appointment), "", 200, new ArrayList<>(),
				new ArrayList<>());
	}

	@GetMapping("/appointment/{id}")
	public WsResponse getAppointmentById(@PathVariable Long id, HttpServletRequest req) throws Exception {
		return GeneralUtility.getData(appointmentService.getAppointmentById(id), "", 200, new ArrayList<>(), new ArrayList<>());
	}

	@DeleteMapping("/appointment/{id}")
	public WsResponse deleteAppointment(@PathVariable Long id, HttpServletRequest req) throws Exception {
		appointmentService.deleteAppointmentById(id);
		return GeneralUtility.getData("", "", 200, new ArrayList<>(), new ArrayList<>());
	}
	
	@GetMapping("/appointments")
	public WsResponse getAllAppointments(HttpServletRequest req) throws Exception {
		return GeneralUtility.getData(appointmentService.getAllAppointments(), "", 200, new ArrayList<>(), new ArrayList<>());
	}

	
}
