package com.das.servuce.impl;

import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.Instant;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.Set;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.das.model.AppointmentSlot;
import com.das.model.Doctor;
import com.das.repository.AppointmentSlotRepository;
import com.das.repository.DoctorRepository;
import com.das.service.DoctorService;

@Service("DoctorService")
@Transactional
public class DoctorServiceImpl implements DoctorService {

	private static final Logger logger = LoggerFactory.getLogger(DoctorServiceImpl.class);

	@Autowired
	private DoctorRepository doctorRepository;
	
	@Autowired
	private AppointmentSlotRepository appointmentSlotRepository;

	// This method is doctor to create new doctors.
	@Override
	public Doctor addDoctor(Doctor doctor) throws Exception {
		return doctorRepository.save(doctor);
	}	

	// method is doctor to edit doctor by id
	@Override
	public Doctor editDoctorById(Doctor doctor) throws Exception {
		Optional<Doctor> existDoctor = null;
		try {
			existDoctor = doctorRepository.findById(doctor.getId());

			doctor.getAppointmentSlot().forEach(availability -> {
				AppointmentSlot timeslot = new AppointmentSlot();
				Set<AppointmentSlot> timeslotList = new HashSet<AppointmentSlot>();
				Instant availableFrom = timeslot.getStartTime();
				Instant availableTo = timeslot.getEndTime();

				Duration timeElapsed = Duration.between(availableFrom, availableTo);

				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

				String fromDate = sdf.format(Date.from(availableFrom));
				String toDate = sdf.format(Date.from(availableTo));
				try {
					Calendar startCalendar = Calendar.getInstance();
					startCalendar.setTime(sdf.parse(fromDate));
					Calendar endCalendar = Calendar.getInstance();
					endCalendar.setTime(sdf.parse(toDate));

					SimpleDateFormat slotTime = new SimpleDateFormat("HH:mm");
					while (endCalendar.after(startCalendar)) {
						String slotStartTime = slotTime.format(startCalendar.getTime());
						startCalendar.add(Calendar.MINUTE, Integer.parseInt(availability.getTimeslotsDuration()));

						String slotEndTime = slotTime.format(startCalendar.getTime());
						
						
						String[] startTimeArrOfStr = slotStartTime.split(":"); 
						String[] endTimeArrOfStr = slotEndTime.split(":");
						Date from = Date.from(availableFrom);
						from.setHours(Integer.parseInt(startTimeArrOfStr[0]));
						from.setMinutes(Integer.parseInt(startTimeArrOfStr[1]));
						String fromDate1 = sdf.format(from);
						Date to = Date.from(availableTo);
						to.setHours(Integer.parseInt(endTimeArrOfStr[0]));
						to.setMinutes(Integer.parseInt(endTimeArrOfStr[1]));
						String toDate1 = sdf.format(to);
						timeslotList.add(timeslot);
					}				
					availability.setTimeslotsDuration(availability.getTimeslotsDuration());
				} catch (Exception e) {
					// TODO: handle exception
				}
			});
			existDoctor.get().setName(doctor.getName());
			existDoctor.get().setDegreeName(doctor.getDegreeName());
			existDoctor.get().setSpecialization(doctor.getSpecialization());
			existDoctor.get().setContactDetail(doctor.getContactDetail());
			existDoctor.get().setAddress(doctor.getAddress());
			doctorRepository.save(existDoctor.get());
			return existDoctor.get();
		} catch (NoSuchElementException e) {
			return existDoctor.get();
		}
	}

	// This method is doctor to delete particular doctor based on id.
	@Override
	public void deleteDoctorById(Long id) throws Exception {

		final Doctor tempDoctor = isExist(id);
		if (tempDoctor != null) {
			doctorRepository.delete(tempDoctor);
		} else {
			throw new Exception("doctor.not.found");
		}
	}

	// This method is doctor to get doctor details.
	@Override
	public Doctor getDoctorById(Long id) throws Exception {
		return isExist(id);
	}

	// check doctor is exit or not
	public Doctor isExist(long id) throws Exception {
		Doctor doctor = null;
		try {
			doctor = doctorRepository.findById(id);
		} catch (final Exception e) {
			e.printStackTrace();
			logger.info("doctor.not.found", e);
		}
		return doctor;
	}

	// Get All doctors
	public List<Doctor> getAllDoctors() throws Exception {
		List<Doctor> doctors = null;
		try {
			doctors = doctorRepository.findAll();
		} catch (Exception e) {
			e.printStackTrace();
			logger.info("error.in.get.doctors", e);
		}
		return doctors;
	}
}