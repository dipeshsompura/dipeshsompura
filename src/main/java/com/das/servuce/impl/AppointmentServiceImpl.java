package com.das.servuce.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.das.model.Appointment;
import com.das.model.Doctor;
import com.das.repository.AppointmentRepository;
import com.das.service.AppointmentService;

@Service("AppointmentService")
@Transactional
public class AppointmentServiceImpl implements AppointmentService {

	private static final Logger logger = LoggerFactory.getLogger(AppointmentServiceImpl.class);

	@Autowired
	private AppointmentRepository appointmentRepository;

	// This method is Appointment to create new Appointments.
	@Override
	public Appointment addAppointment(Appointment appointment) throws Exception {
		return appointmentRepository.save(appointment);
	}	

	// method is Appointment to edit Appointment by id
	@Override
	public Appointment editAppointmentById(Appointment appointment) throws Exception {

		final Appointment tempAppointment = isExist(appointment.getId());

		if (tempAppointment != null) {
			try {
				tempAppointment.setStartTime(appointment.getStartTime());
				tempAppointment.setEndTime(appointment.getEndTime());
				tempAppointment.setDoctors(appointment.getDoctors());
				tempAppointment.setPatient(appointment.getPatient());
				appointmentRepository.save(tempAppointment);
			} catch (Exception e) {
				e.printStackTrace();
				logger.info("error in updation", e);
			}

		} else {
			throw new Exception("Appointment.not.found");
		}

		return tempAppointment;
	}

	// This method is Appointment to delete particular Appointment based on id.
	@Override
	public void deleteAppointmentById(Long id) throws Exception {

		final Appointment tempAppointment = isExist(id);
		if (tempAppointment != null) {
			appointmentRepository.delete(tempAppointment);
		} else {
			throw new Exception("Appointment.not.found");
		}
	}

	// This method is Appointment to get Appointment details.
	@Override
	public Appointment getAppointmentById(Long id) throws Exception {
		return isExist(id);
	}

	// check user is exit or not
	public Appointment isExist(long id) throws Exception {
		Appointment appointment = null;
		try {
			appointment = appointmentRepository.findById(id);
		} catch (final Exception e) {
			e.printStackTrace();
			logger.info("appointment.not.found", e);
		}
		return appointment;
	}

	// Get All Appointments
	public List<Appointment> getAllAppointments() throws Exception {
		List<Appointment> appointments = null;
		try {
			appointments = appointmentRepository.findAll();
		} catch (Exception e) {
			e.printStackTrace();
			logger.info("error.in.get.appointments", e);
		}
		return appointments;
	}
}