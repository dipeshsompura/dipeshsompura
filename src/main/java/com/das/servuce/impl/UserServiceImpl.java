package com.das.servuce.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.das.model.User;
import com.das.repository.UserRepository;
import com.das.service.UserService;

@Service("UserService")
@Transactional
public class UserServiceImpl implements UserService {

	private static final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

	@Autowired
	private UserRepository userRepository;

	// This method is user to create new users.
	@Override
	public User addUser(User user) throws Exception {
		User usserObj = new User();
		if (!isUserExist(user.getEmail())) {			
			try {
				usserObj.setName(user.getName());
				usserObj.setEmail(user.getEmail());
				usserObj.setMobile(user.getMobile());
				usserObj.setGender(user.getGender());
				// here we required to put encript password, but i skip this step
				usserObj.setPassword(user.getPassword());
				usserObj.setUserType(user.getUserType());
				if (user.getUserType().equalsIgnoreCase("Doctor")) {
					usserObj.setDoctor(user.getDoctor());
				}
				if (user.getUserType().equalsIgnoreCase("Patient")) {
					usserObj.setPatient(user.getPatient());
				}
				userRepository.save(user);
			} catch (Exception e) {
				e.printStackTrace();
				logger.info("error in registration", e);
			}
		}
		return user;
	}

	// This method is used to check if email id(user) is already exist or not.
	public boolean isUserExist(String userEmailId) throws Exception {

		final User user = userRepository.findByEmail(userEmailId);
		if (user != null) {
			throw new Exception("duplicate.username");
		}

		return false;
	}

	// method is used to edit user by id
	@Override
	public User editUserById(User user) throws Exception {

		final User tempuser = isExist(user.getId());

		if (tempuser != null) {
			try {
				tempuser.setName(user.getName());
				tempuser.setEmail(user.getEmail());
				tempuser.setMobile(user.getMobile());
				tempuser.setGender(user.getGender());
				userRepository.save(tempuser);
			} catch (Exception e) {
				e.printStackTrace();
				logger.info("error in updation", e);
			}

		} else {
			throw new Exception("user.not.found");
		}

		return tempuser;
	}

	// This method is used to delete particular user based on id.
	@Override
	public void deleteUserById(Long id) throws Exception {

		final User tempuser = isExist(id);
		if (tempuser != null) {
			userRepository.delete(tempuser);
		} else {
			throw new Exception("user.not.found");
		}
	}

	// This method is used to get user details.
	@Override
	public User getUserById(Long id) throws Exception {
		return isExist(id);
	}

	// check user is exit or not
	public User isExist(long id) throws Exception {
		User user = null;
		try {
			user = userRepository.findById(id);
		} catch (final Exception e) {
			e.printStackTrace();
			logger.info("user.not.found", e);
		}
		return user;
	}

	// User login
	@Override
	public User login(User userRequest) throws Exception {
		final User user = userRepository.findByEmailAndPassword(userRequest.getEmail(), userRequest.getPassword());
		if (user == null) {
			throw new Exception("Username and Password is not correct plz try again");
		}
		return user;
	}
	
	// Get All Users
	public List<User> getAllUsers() throws Exception {
		List<User> users = null;
		try {
			users = userRepository.findAll();
		} catch (Exception e) {
			e.printStackTrace();
			logger.info("error.in.get.user", e);
		}
		return users;
	}
}