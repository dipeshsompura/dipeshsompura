package com.das.servuce.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.das.model.Doctor;
import com.das.model.Patient;
import com.das.repository.PatientRepository;
import com.das.service.PatientService;

@Service("PatientService")
@Transactional
public class PatientServiceImpl implements PatientService {

	private static final Logger logger = LoggerFactory.getLogger(PatientServiceImpl.class);

	@Autowired
	private PatientRepository patientRepository;

	// This method is patient to create new patient.
	@Override
	public Patient addPatient(Patient patient) throws Exception {
		return patientRepository.save(patient);
	}	

	// method is patient to edit patient by id
	@Override
	public Patient editPatientById(Patient patient) throws Exception {

		final Patient tempDoctor = isExist(patient.getId());

		if (tempDoctor != null) {
			try {
				tempDoctor.setName(patient.getName());
				tempDoctor.setAge(patient.getAge());
				tempDoctor.setGender(patient.getGender());
				tempDoctor.setBloodgroup(patient.getBloodgroup());
				tempDoctor.setMobileNumber(patient.getMobileNumber());
				patientRepository.save(tempDoctor);
			} catch (Exception e) {
				e.printStackTrace();
				logger.info("error in updation", e);
			}

		} else {
			throw new Exception("Patient.not.found");
		}

		return tempDoctor;
	}

	// This method is Patient to delete particular Patient based on id.
	@Override
	public void deletePatientById(Long id) throws Exception {

		final Patient tempPatient = isExist(id);
		if (tempPatient != null) {
			patientRepository.delete(tempPatient);
		} else {
			throw new Exception("Patient.not.found");
		}
	}

	// This method is patient to get patient details.
	@Override
	public Patient getPatientById(Long id) throws Exception {
		return isExist(id);
	}

	// check user is exit or not
	public Patient isExist(long id) throws Exception {
		Patient patient = null;
		try {
			patient = patientRepository.findById(id);
		} catch (final Exception e) {
			e.printStackTrace();
			logger.info("patient.not.found", e);
		}
		return patient;
	}

	// Get All patients
	public List<Patient> getAllPatients() throws Exception {
		List<Patient> patients = null;
		try {
			patients = patientRepository.findAll();
		} catch (Exception e) {
			e.printStackTrace();
			logger.info("error.in.get.patients", e);
		}
		return patients;
	}
}