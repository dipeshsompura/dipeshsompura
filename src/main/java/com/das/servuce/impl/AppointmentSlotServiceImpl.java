package com.das.servuce.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.das.model.AppointmentSlot;
import com.das.model.Doctor;
import com.das.repository.AppointmentSlotRepository;
import com.das.service.AppointmentSlotService;

@Service("AppointmentSlotService")
@Transactional
public class AppointmentSlotServiceImpl implements AppointmentSlotService {

	private static final Logger logger = LoggerFactory.getLogger(AppointmentSlotServiceImpl.class);

	@Autowired
	private AppointmentSlotRepository AppointmentSlotRepository;

	// This method is appointmentSlot to create new appointmentSlots.
	@Override
	public AppointmentSlot addAppointmentSlot(AppointmentSlot appointmentSlot) throws Exception {
		return AppointmentSlotRepository.save(appointmentSlot);
	}	

	// method is appointmentSlot to edit appointmentSlot by id
	@Override
	public AppointmentSlot editAppointmentSlotById(AppointmentSlot appointmentSlot) throws Exception {

		final AppointmentSlot tempAppointmentSlot = isExist(appointmentSlot.getId());

		if (tempAppointmentSlot != null) {
			try {
				tempAppointmentSlot.setStartTime(appointmentSlot.getStartTime());
				tempAppointmentSlot.setEndTime(appointmentSlot.getEndTime());
				tempAppointmentSlot.setDoctor(appointmentSlot.getDoctor());
				AppointmentSlotRepository.save(tempAppointmentSlot);
			} catch (Exception e) {
				e.printStackTrace();
				logger.info("error in updation", e);
			}

		} else {
			throw new Exception("AppointmentSlot.not.found");
		}

		return tempAppointmentSlot;
	}

	// This method is appointmentSlot to delete particular appointmentSlot based on id.
	@Override
	public void deleteAppointmentSlotById(Long id) throws Exception {

		final AppointmentSlot tempAppointmentSlot = isExist(id);
		if (tempAppointmentSlot != null) {
			AppointmentSlotRepository.delete(tempAppointmentSlot);
		} else {
			throw new Exception("AppointmentSlot.not.found");
		}
	}

	// This method is appointmentSlot to get appointmentSlot details.
	@Override
	public AppointmentSlot getAppointmentSlotById(Long id) throws Exception {
		return isExist(id);
	}

	// check user is exit or not
	public AppointmentSlot isExist(long id) throws Exception {
		AppointmentSlot appointmentSlot = null;
		try {
			appointmentSlot = AppointmentSlotRepository.findById(id);
		} catch (final Exception e) {
			e.printStackTrace();
			logger.info("appointmentSlot.not.found", e);
		}
		return appointmentSlot;
	}

	// Get All appointmentSlot
	public List<AppointmentSlot> getAllAppointmentSlots() throws Exception {
		List<AppointmentSlot> appointmentSlots = null;
		try {
			appointmentSlots = AppointmentSlotRepository.findAll();
		} catch (Exception e) {
			e.printStackTrace();
			logger.info("error.in.get.appointmentSlots", e);
		}
		return appointmentSlots;
	}
}