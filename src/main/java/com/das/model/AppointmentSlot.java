package com.das.model;

import java.time.Instant;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.das.common.BaseModel;

@Entity
@Table(name = "appointmentSlot")
public class AppointmentSlot extends BaseModel {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private Instant startTime;

	private Instant endTime;
	
	private String timeslotsDuration;

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "doctors_id")
    private Doctor doctor;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Instant getStartTime() {
		return startTime;
	}

	public void setStartTime(Instant startTime) {
		this.startTime = startTime;
	}

	public Instant getEndTime() {
		return endTime;
	}

	public void setEndTime(Instant endTime) {
		this.endTime = endTime;
	}

	public Doctor getDoctor() {
		return doctor;
	}

	public void setDoctor(Doctor doctor) {
		this.doctor = doctor;
	}

	public String getTimeslotsDuration() {
		return timeslotsDuration;
	}

	public void setTimeslotsDuration(String timeslotsDuration) {
		this.timeslotsDuration = timeslotsDuration;
	}

	
	
}